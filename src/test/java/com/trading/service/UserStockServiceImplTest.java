package com.trading.service;

import static org.mockito.Mockito.doReturn;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.trading.dto.UserStockRequestDto;
import com.trading.entity.User;
import com.trading.entity.UserStock;
import com.trading.exception.UserNotFoundException;
import com.trading.exception.UserStockNotFoundException;
import com.trading.feignclient.StockServiceFeignClient;
import com.trading.feignclient.dto.Stock;
import com.trading.repository.UserRepository;
import com.trading.repository.UserStockRespository;

public class UserStockServiceImplTest {

	@InjectMocks
	UserStockServiceImpl userStockServiceImpl;
	@Mock
	UserRepository userRepository;
	@Mock
	StockServiceFeignClient feignClient;
	@Mock
	UserStockRespository userStockRepository;

	@BeforeEach
	public void dosetups() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testTrading() {
		UserStockRequestDto requestDto = new UserStockRequestDto();
		requestDto.setUserId(1);
		requestDto.setAction("BUY");
		Optional<User> user = Optional.of(new User(1, "abc", "abc", new BigInteger("1234567890"), "abc"));
		ResponseEntity<Stock> responseStk = new ResponseEntity<>(HttpStatus.OK);
		UserStock userStockObj = new UserStock();
		userStockObj.setUserStockId(1);
		try {
			doReturn(user).when(userRepository).findById(1);
			doReturn(responseStk).when(feignClient).getStockById(1);
			doReturn(userStockObj).when(userStockRepository).findBoughtUserStockWithinTwentyFourHours(1, 1);
			doReturn(userStockObj).when(userStockRepository).findSoldUserStockWithinTwentyFourHours(1, 1);
			userStockServiceImpl.trading(requestDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testgetUserHoldingStocks() {
		Optional<User> user = Optional.of(new User(1, "abc", "abc", new BigInteger("1234567890"), "abc"));
		List<UserStock> userStockList = new ArrayList<UserStock>();
		UserStock us = new UserStock();
		us.setUserStockId(1);
		userStockList.add(us);
		try {
			doReturn(user).when(userRepository).findById(1);
			doReturn(userStockList).when(userStockRepository).findUserStocksWithinTwentyFourHours(1);
			userStockServiceImpl.getUserHoldingStocks(1);
		} catch (UserNotFoundException e) {
			e.printStackTrace();
		} catch (UserStockNotFoundException e) {
			e.printStackTrace();
		}
	}

}
