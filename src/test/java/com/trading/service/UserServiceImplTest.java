package com.trading.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.trading.constants.AppConstants;
import com.trading.dto.UserLoginRequestDto;
import com.trading.dto.UserLoginResponseDto;
import com.trading.entity.User;
import com.trading.repository.UserRepository;

public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepository;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationSuccess() {
		UserLoginRequestDto loginRequestDto = new UserLoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test@test.com");
		user.setPassword("123456");
		when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);

		UserLoginResponseDto userLoginResponseDto = userService.userLogin(loginRequestDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, userLoginResponseDto.getStatus());
		assertEquals(AppConstants.STATUS_CODE_SUCCESS, userLoginResponseDto.getStatusCode());
	}
	
	
	@Test
	public void testLoginValidationFailure() {
		UserLoginRequestDto loginRequestDto = new UserLoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test1@test1.com");
		user.setPassword("123456");
		when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);

		UserLoginResponseDto userLoginResponseDto = userService.userLogin(loginRequestDto);
		assertEquals(AppConstants.LOGIN_FAILURE, userLoginResponseDto.getStatus());
		assertEquals(AppConstants.STATUS_CODE_FAILURE, userLoginResponseDto.getStatusCode());
	}

}
