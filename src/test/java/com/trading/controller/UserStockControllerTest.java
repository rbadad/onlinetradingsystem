package com.trading.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.trading.dto.UserStockRequestDto;
import com.trading.dto.UserStockResponseDto;
import com.trading.service.UserStockService;

@SpringBootTest
class UserStockControllerTest {

	@InjectMocks
	private UserStockController userStockController;
	
	@Mock
	private UserStockService userStockService;
	
	@Before(value = "")
	public void doSetups() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	void testtrading() {
		UserStockRequestDto requestDto = new UserStockRequestDto();
		requestDto.setAction("BUY");
		requestDto.setStockId(1);
		requestDto.setTotalPrice(123.123);
		requestDto.setTotalQuantity(1);
		requestDto.setUserId(1);
		UserStockResponseDto responseDto = new UserStockResponseDto();
		try {
			doReturn(responseDto).when(userStockService).trading(requestDto);
			userStockController.trading(requestDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testgetUserHoldingStocks() {
		try {
			doNothing().when(userStockService).getUserHoldingStocks(1);
			userStockController.getUserHoldingStocks(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
