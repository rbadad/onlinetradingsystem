package com.trading.controller;

import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.trading.dto.StockResponseDTO;
import com.trading.feignclient.StockServiceFeignClient;
import com.trading.feignclient.dto.Stock;

@SpringBootTest
class StockControllerTest {
	
	@InjectMocks
	private StockController stockController;
	
	@Mock
	private StockServiceFeignClient stockService;
	
	@Before(value = "")
	public void setups() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getStockstest() {
		List<Stock> stocks = new ArrayList<Stock>();
		Stock stock = new Stock();
		stock.setStockId(1);
		stock.setStockName("TATA");
		stock.setStockPrice(123.123);
		stocks.add(stock);
		try {
			doReturn(stocks).when(stockService).getStocks();
			stockController.getStocks();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void getStockByIdtest() {
		StockResponseDTO sdto = new StockResponseDTO();
		sdto.setStockId(1);
		sdto.setQuantity(123);
		sdto.setStockName("qwerty");
		sdto.setStockPrice(123.123);
		sdto.setTrend("good");
		try {
			doReturn(sdto).when(stockService).getStockById(1);
			((StockServiceFeignClient) stockController).getStockById(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
