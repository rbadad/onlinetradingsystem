package com.trading.controller;

import static org.mockito.Mockito.doReturn;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.trading.dto.UserLoginRequestDto;
import com.trading.dto.UserLoginResponseDto;
import com.trading.entity.User;
import com.trading.service.UserService;
import com.trading.service.UserServiceImpl;

@SpringBootTest
public class UserControllerTest {

	@InjectMocks
	private UserController userController;
	
	@Mock
	private UserServiceImpl userServiceImpl;
	
	@Mock
	private UserService userService;
	
	@Before(value = "")
	public void dosetups() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void userLoginTest() {
		UserLoginResponseDto userloginresp = new UserLoginResponseDto();
		UserLoginRequestDto userloginDto = new UserLoginRequestDto();
		userloginDto.setEmail("raj@gmail.com");
		userloginDto.setPassword("raj123");
		User user = new User();
		user.setUserId(1);
		doReturn(userloginresp).when(userServiceImpl).userLogin(userloginDto);
		userController.userLogin(userloginDto);
	}
}

