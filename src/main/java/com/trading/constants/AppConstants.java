package com.trading.constants;

public class AppConstants {

	public static final String LOGIN_SUCCESS = "LOGIN SUCCESSFUL";
	public static final String LOGIN_FAILURE = "INVALID EMAIL_ID OR PASSWORD";
	public static final String STATUS_CODE_SUCCESS = "1";
	public static final String STATUS_CODE_FAILURE = "0";
	public static final String USER_NOT_FOUND = " User is not available";
	public static final String STOCK_NOT_FOUND = " Stock not found";
	public static final String USERSTOCK_NOT_FOUND = "none of the Stock is bought or sold within 24hours";
	public static final String TRADING_SUCCESS = "Bought/ sold stock successfully";
	public static final String TRADING_SUCCESS_CODE = "100";

	private AppConstants() {
	}

	public static final String CAN_NOT_SOLD_STOCK = "Can not sell stock which bought within 24 hours";
	public static final String CAN_NOT_BUY_STOCK = "Can not buy stock which sold within 24 hours";
	public static final String BUY = "BUY";
	public static final String SELL = "SELL";

	public static final String USER_NOT_FOUND_STATUS_CODE ="600";
	public static final String STOCK_NOT_FOUND_STATUS_CODE ="601";
	public static final String CAN_NOT_SOLD_STOCK_STATUS_CODE ="602";
	public static final String CAN_NOT_BUY_STOCK_STATUS_CODE ="603";
	public static final String USERSTOCK_NOT_FOUND_STATUS_CODE = "604";
}
