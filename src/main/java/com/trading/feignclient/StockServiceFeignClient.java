package com.trading.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.trading.exception.StockNotFoundException;
import com.trading.feignclient.dto.Stock;

@FeignClient(value = "Stock-Service", url = "http://localhost:2021/stock/stocks")
public interface StockServiceFeignClient {

	@GetMapping
	public ResponseEntity<List<Stock>> getStocks() throws StockNotFoundException;

	@GetMapping("/{stockId}")
	public ResponseEntity<Stock> getStockById(@PathVariable("stockId") Integer stockId) throws StockNotFoundException;
}