package com.trading.exception;

public class UserStockNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserStockNotFoundException() {
		super();
	}

	public UserStockNotFoundException(String msg) {
		super(msg);
	}

}
