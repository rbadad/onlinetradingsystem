package com.trading.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.trading.constants.AppConstants;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<String> handleUserNotFoundException() {
		return new ResponseEntity<>(AppConstants.USER_NOT_FOUND,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = StockNotFoundException.class)
	public ResponseEntity<String> handleStockNotFoundException() {
		return new ResponseEntity<>(AppConstants.STOCK_NOT_FOUND,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = UserStockNotFoundException.class)
	public ResponseEntity<String> handleUserStockNotFoundException() {
		return new ResponseEntity<>(AppConstants.USERSTOCK_NOT_FOUND,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = SoldUserStockException.class)
	public ResponseEntity<String> handleSoldUserStockException() {
		return new ResponseEntity<>(AppConstants.CAN_NOT_SOLD_STOCK,HttpStatus.OK);
	}
	
	@ExceptionHandler(value = BuyUserStockException.class)
	public ResponseEntity<String> handleBuyUserStockException() {
		return new ResponseEntity<>(AppConstants.CAN_NOT_BUY_STOCK,HttpStatus.OK);
	}
}
