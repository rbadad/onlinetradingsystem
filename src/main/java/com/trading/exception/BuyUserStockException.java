package com.trading.exception;

public class BuyUserStockException extends Exception {

	private static final long serialVersionUID = 1L;

	public BuyUserStockException() {
		super();
	}

	public BuyUserStockException(String message) {
		super(message);
	}

}
