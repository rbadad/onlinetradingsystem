package com.trading.exception;

public class SoldUserStockException extends Exception {

	private static final long serialVersionUID = 1L;

	public SoldUserStockException() {
		super();
	}

	public SoldUserStockException(String message) {
		super(message);
	}

}
