package com.trading.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.trading.entity.UserStock;

@Repository
public interface UserStockRespository extends JpaRepository<UserStock, Integer>{

	@Query(value = "select * from user_stock  where user_id=:userId and (buy_date > DATE_SUB(NOW(), INTERVAL 24 HOUR) or sold_date > DATE_SUB(NOW(), INTERVAL 24 HOUR))",nativeQuery = true)
	public List<UserStock> findUserStocksWithinTwentyFourHours(Integer userId);
	
	@Query(value ="select * from user_stock where user_id=:userId and stock_id=:stockId and action= 'BUY' and buy_date > DATE_SUB(NOW(), INTERVAL 24 HOUR)", nativeQuery = true)
	public UserStock findBoughtUserStockWithinTwentyFourHours(Integer userId, Integer stockId);
	
	@Query(value ="select * from user_stock where user_id=:userId and stock_id=:stockId and action= 'SELL' and sold_date > DATE_SUB(NOW(), INTERVAL 24 HOUR)", nativeQuery = true)
	public UserStock findSoldUserStockWithinTwentyFourHours(Integer userId, Integer stockId);

}
