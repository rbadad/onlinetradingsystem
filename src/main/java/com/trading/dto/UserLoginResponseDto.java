package com.trading.dto;

public class UserLoginResponseDto {
	
	private String statusCode;
	private String status;
	public UserLoginResponseDto() {
		super();
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
