package com.trading.controller;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trading.dto.UserLoginRequestDto;
import com.trading.dto.UserLoginResponseDto;
import com.trading.service.UserService;

@RestController
@RequestMapping("/userlogin")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<UserLoginResponseDto> userLogin(@RequestBody UserLoginRequestDto userloginDto) {

		UserLoginResponseDto userLoginResponseDto = new UserLoginResponseDto();
		if (Objects.isNull(userloginDto.getEmail())) {
			userLoginResponseDto.setStatus("User email should not be blank");
			userLoginResponseDto.setStatusCode("0");
		}
		if (Objects.isNull(userloginDto.getPassword())) {
			userLoginResponseDto.setStatus("User password should not be blank");
			userLoginResponseDto.setStatusCode("0");
		}

		try {
			userLoginResponseDto = userService.userLogin(userloginDto);
		} catch (Exception e) {
			LOGGER.info("Invalid User Credentials");
		}
		return new ResponseEntity<>(userLoginResponseDto, HttpStatus.OK);
	}
}
