package com.trading.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trading.exception.StockNotFoundException;
import com.trading.feignclient.StockServiceFeignClient;
import com.trading.feignclient.dto.Stock;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/stocks")
public class StockController {
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

	@Autowired
	StockServiceFeignClient stockFeignClient;

	@GetMapping
	@ApiOperation("Stocks Information")
	public ResponseEntity<List<Stock>> getStocks() throws StockNotFoundException {
		LOGGER.debug("Stocks Controller :: start");
		ResponseEntity<List<Stock>> stocks = stockFeignClient.getStocks();
		LOGGER.debug("Stocks Controller :: end");
		return new ResponseEntity<>(stocks.getBody(), HttpStatus.OK);
	}

}
