package com.trading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.trading.dto.UserStockRequestDto;
import com.trading.dto.UserStockResponseDto;
import com.trading.exception.BuyUserStockException;
import com.trading.exception.SoldUserStockException;
import com.trading.exception.StockNotFoundException;
import com.trading.exception.UserNotFoundException;
import com.trading.exception.UserStockNotFoundException;
import com.trading.service.UserStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Operations pertaining to user-stocks")
@RestController
@RequestMapping("user-stocks")
public class UserStockController {

	@Autowired
	UserStockService userStockService;

	@ApiOperation("buy or sell the stocks ")
	@PostMapping("/")
	public ResponseEntity<UserStockResponseDto> trading(@RequestBody UserStockRequestDto requestDto)
			throws UserNotFoundException, StockNotFoundException, SoldUserStockException, BuyUserStockException {
		UserStockResponseDto responseDto = userStockService.trading(requestDto);
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}

	@ApiOperation("User holding stocks")
	@GetMapping("/{userId}")
	public ResponseEntity<Object> getUserHoldingStocks(@PathVariable Integer userId)
			throws UserNotFoundException, UserStockNotFoundException {
		Object obj = userStockService.getUserHoldingStocks(userId);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}
}
