package com.trading.service;

import com.trading.dto.UserStockRequestDto;
import com.trading.dto.UserStockResponseDto;
import com.trading.exception.BuyUserStockException;
import com.trading.exception.SoldUserStockException;
import com.trading.exception.StockNotFoundException;
import com.trading.exception.UserNotFoundException;
import com.trading.exception.UserStockNotFoundException;

public interface UserStockService {

	public UserStockResponseDto trading(UserStockRequestDto requestDto) throws UserNotFoundException,StockNotFoundException,SoldUserStockException,BuyUserStockException;
	
	public  Object getUserHoldingStocks(Integer userId) throws UserNotFoundException,UserStockNotFoundException;
}
