package com.trading.service;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trading.constants.AppConstants;
import com.trading.controller.UserController;
import com.trading.dto.UserLoginRequestDto;
import com.trading.dto.UserLoginResponseDto;
import com.trading.entity.User;
import com.trading.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserLoginResponseDto userLogin(UserLoginRequestDto userLoginDto) {
		UserLoginResponseDto response = new UserLoginResponseDto();
		try {
			User user = userRepository.findByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword());
			if (!Objects.isNull(user)) {
				response.setStatusCode(AppConstants.STATUS_CODE_SUCCESS);
				response.setStatus(AppConstants.LOGIN_SUCCESS);
			} else {
				response.setStatusCode(AppConstants.STATUS_CODE_FAILURE);
				response.setStatus(AppConstants.LOGIN_FAILURE);
			}
		} catch (Exception e) {
			LOGGER.info("Invalid User Credentials");
		}
		return response;
	}

}
