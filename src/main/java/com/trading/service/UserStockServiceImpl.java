package com.trading.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.trading.constants.AppConstants;
import com.trading.dto.UserStockRequestDto;
import com.trading.dto.UserStockResponseDto;
import com.trading.entity.User;
import com.trading.entity.UserStock;
import com.trading.exception.BuyUserStockException;
import com.trading.exception.SoldUserStockException;
import com.trading.exception.StockNotFoundException;
import com.trading.exception.UserNotFoundException;
import com.trading.exception.UserStockNotFoundException;
import com.trading.feignclient.StockServiceFeignClient;
import com.trading.feignclient.dto.Stock;
import com.trading.repository.UserRepository;
import com.trading.repository.UserStockRespository;

import feign.FeignException;

@Service
public class UserStockServiceImpl implements UserStockService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	StockServiceFeignClient feignClient;
	@Autowired
	UserStockRespository userStockRepository;


	@Override
	public UserStockResponseDto trading(UserStockRequestDto requestDto) throws UserNotFoundException, StockNotFoundException, SoldUserStockException, BuyUserStockException {
		UserStockResponseDto responseDto = new UserStockResponseDto();
		Integer userId = requestDto.getUserId();
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent()) {
			responseDto.setStatus(AppConstants.USER_NOT_FOUND);
			responseDto.setStatusCode(AppConstants.USER_NOT_FOUND_STATUS_CODE);
			return responseDto;
		}
		Integer stockId = requestDto.getStockId();
		try {
			feignClient.getStockById(stockId);
		} catch(FeignException fe) {
			responseDto.setStatus(AppConstants.STOCK_NOT_FOUND);
			responseDto.setStatusCode(AppConstants.STOCK_NOT_FOUND_STATUS_CODE);
			return responseDto;
		}

		UserStock userStock = new UserStock();
		String action = requestDto.getAction();
		if(action.equals(AppConstants.SELL)) {
			UserStock userStockObj = userStockRepository.findBoughtUserStockWithinTwentyFourHours(userId, stockId);
			if(userStockObj!=null) {
				responseDto.setStatus(AppConstants.CAN_NOT_SOLD_STOCK);
				responseDto.setStatusCode(AppConstants.CAN_NOT_SOLD_STOCK_STATUS_CODE);
				return responseDto;
			} else {
				userStock.setSoldDate(LocalDate.now());
			}
		}
		if(action.equals(AppConstants.BUY)) {
			UserStock userStockObj = userStockRepository.findSoldUserStockWithinTwentyFourHours(userId, stockId);
			if(userStockObj!=null) {
				responseDto.setStatus(AppConstants.CAN_NOT_BUY_STOCK);
				responseDto.setStatusCode(AppConstants.CAN_NOT_BUY_STOCK_STATUS_CODE);
				return responseDto;
			} else {
				userStock.setBuyDate(LocalDate.now());
			}
		}

		userStock.setAction(action);
		userStock.setUserId(userId);
		userStock.setStockId(requestDto.getStockId());
		userStock.setTotalPrice(requestDto.getTotalPrice());
		userStock.setTotalQuantity(requestDto.getTotalQuantity());
		userStockRepository.save(userStock);
		responseDto.setStatus(AppConstants.TRADING_SUCCESS);
		responseDto.setStatusCode(AppConstants.TRADING_SUCCESS_CODE);
		return  responseDto;
	}


	@Override
	public Object getUserHoldingStocks(Integer userId) throws UserNotFoundException, UserStockNotFoundException {
		UserStockResponseDto responseDto = new UserStockResponseDto();
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent()) {
			responseDto.setStatus(AppConstants.USER_NOT_FOUND);
			responseDto.setStatusCode(AppConstants.USER_NOT_FOUND_STATUS_CODE);
			return responseDto;
		}
		List<UserStock> userStockList = userStockRepository.findUserStocksWithinTwentyFourHours(userId);
		if(userStockList.isEmpty()) {
			responseDto.setStatus(AppConstants.USERSTOCK_NOT_FOUND);
			responseDto.setStatusCode(AppConstants.USERSTOCK_NOT_FOUND_STATUS_CODE);
			return responseDto;
		}
		return userStockList;
	}
}
