package com.trading.service;

import com.trading.dto.UserLoginRequestDto;
import com.trading.dto.UserLoginResponseDto;


public interface UserService {
	
	public UserLoginResponseDto userLogin(UserLoginRequestDto userLoginDto);

}
